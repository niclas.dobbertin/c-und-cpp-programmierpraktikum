#include "Box.hpp"
#include "Person.hpp"
#include "Student.hpp"
#include <iostream>

void printPersonInfo(const Person* person){
    std::cout << person->getInfo() << std::endl;
}

int main() {
    /*
    Box oneBox;
    oneBox.set_dimensions(5, 6);
    Box anotherBox = oneBox;
    anotherBox.set_dimensions(3, 4);
    oneBox.print_dimensions();
    anotherBox.print_dimensions(); */

    /*
    Person testPerson("Tobias");
    std::cout << testPerson.getInfo() << std::endl;
    Student testStudent("Mark", "0123456789");
    std::cout << testStudent.getInfo() << std::endl;
    printPersonInfo(&testPerson);
    printPersonInfo(&testStudent); */

    Person* pTim = new Student("Tim", "321654");
    delete pTim;

    return 0;
}
