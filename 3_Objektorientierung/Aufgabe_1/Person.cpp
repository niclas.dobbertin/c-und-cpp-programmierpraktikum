#include "Person.hpp"
#include <iostream>

Person::Person(const std::string &name): name(name){
    std::cout << "Person mit dem Namen " << name << " erstellt." << std::endl;
}

Person::~Person(){
    std::cout << "Person mit dem Namen " << name << " geloescht." << std::endl;
}

std::string Person::getInfo() const {
    return name;
}