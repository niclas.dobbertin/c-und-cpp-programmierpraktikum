#pragma once

class Box {
public:
    int width;
    int* height;
    Box();
    Box(const Box& other);
    Box& operator=(const Box& other);
    ~Box();
    void set_dimensions(int width, int height);
    void print_dimensions();
};
