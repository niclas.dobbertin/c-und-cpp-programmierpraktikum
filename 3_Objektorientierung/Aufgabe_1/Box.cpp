#include "Box.hpp"
#include <iostream>

Box::Box() {
    height = new int();
}

Box::Box(const Box& other) {
    width = other.width;
    height = new int(*other.height);
}

Box& Box::operator=(const Box& other) {
    height = new int(*other.height);
    width = other.width;
    return *this;
}

Box::~Box() {
    delete height;
}

void Box::set_dimensions(int width, int height) {
    this->width = width;
    *(this->height) = height;
}

void Box::print_dimensions() {
    std::cout << "width: "<< width << " | height: " << *height << std::endl;
}