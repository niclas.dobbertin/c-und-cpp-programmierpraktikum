#include "PlusExpression.hpp"
#include <iostream>

PlusExpression::PlusExpression(Expression *left, Expression *right) : BinaryExpression(left, right) {
    std::cout << "Plus Operation " << count << " erstellt" << std::endl;
}

PlusExpression::~PlusExpression(){
    std::cout << "Plus Operation " << count << " geloescht" << std::endl;
}

double PlusExpression::compute(){
    return left->compute() + right->compute();
}

int PlusExpression::count = 0;