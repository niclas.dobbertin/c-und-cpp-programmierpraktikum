#include "MinusExpression.hpp"
#include <iostream>

MinusExpression::MinusExpression(Expression *left, Expression *right) : BinaryExpression(left, right) {
    std::cout << "Minus Operation " << count << " erstellt" << std::endl;
}

MinusExpression::~MinusExpression(){
    std::cout << "Minus Operation " << count << " geloescht" << std::endl;
}

double MinusExpression::compute(){
    return left->compute() - right->compute();
}

int MinusExpression::count = 0;