#include <iostream>
#include "NumberExpression.hpp"
#include "PlusExpression.hpp"
#include "MinusExpression.hpp"

int main() {
    NumberExpression n1(2);
    NumberExpression n2(3);
    PlusExpression p1(&n1, &n2);
    NumberExpression n3 = p1.compute();
    std::cout << n3.compute() << std::endl;
    MinusExpression m1(&n2, &n1);
    NumberExpression n4 = m1.compute();
    std::cout << n4.compute() << std::endl;
    PlusExpression p5(&n3, &n4);
    NumberExpression n5 = p5.compute();
    std::cout << n5.compute() << std::endl;

    return 0;
}
