#pragma once
#include "BinaryExpression.hpp"

class MinusExpression: public BinaryExpression{
public:
    MinusExpression(Expression *left, Expression *right);
    virtual ~MinusExpression();
    virtual double compute();
private:
    static int count;
};

