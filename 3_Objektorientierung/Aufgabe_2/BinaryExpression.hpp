#pragma once
#include "Expression.hpp"

class BinaryExpression: public Expression {
public:
    BinaryExpression(Expression *left, Expression *right);
    virtual ~BinaryExpression();
protected:
    Expression *left;
    Expression *right;
private:
    static int count;
};
