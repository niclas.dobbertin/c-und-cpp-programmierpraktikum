#pragma once
#include "Expression.hpp"

class NumberExpression: public Expression{
public:
    NumberExpression(double number);
    virtual ~NumberExpression();
    virtual double compute();
private:
    double number;
};

