#include "BinaryExpression.hpp"
#include <iostream>

BinaryExpression::BinaryExpression(Expression *left, Expression *right): left(left), right(right){
    std::cout << "Ausdruck " << count << " erstellt" << std::endl;
}

BinaryExpression::~BinaryExpression(){
    std::cout << "Ausdruck " << count << " geloescht" << std::endl;
}

int BinaryExpression::count = 0;