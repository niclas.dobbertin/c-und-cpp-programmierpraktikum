#include "NumberExpression.hpp"
#include <iostream>

NumberExpression::NumberExpression(double number): number(number){
    std::cout << "Nummer " << number << " erstellt" << std::endl;
}

NumberExpression::~NumberExpression(){
    std::cout << "Nummer " << number << " geloescht" << std::endl;
}
double NumberExpression::compute(){
    return number;
}