#pragma once
class Expression {
public:
    Expression();
    virtual ~Expression();
    virtual double compute() = 0;
private:
    static int count;
};

