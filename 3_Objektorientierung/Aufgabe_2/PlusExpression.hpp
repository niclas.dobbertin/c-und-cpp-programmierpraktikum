#pragma once
#include "BinaryExpression.hpp"

class PlusExpression: public BinaryExpression{
public:
    PlusExpression(Expression *left, Expression *right);
    virtual ~PlusExpression();
    virtual double compute();
private:
    static int count;
};
