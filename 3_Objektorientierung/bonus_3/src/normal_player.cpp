#include "normal_player.hpp"
#include <cstdlib>

NormalPlayer::NormalPlayer(Color color) : PerfectPlayer(color), RandomPlayer(color), Player(color) {}

void NormalPlayer::performNextMove(Board& board) {
    int select = rand()%2;
    if (select == 0){
        PerfectPlayer::performNextMove(board);
    }else{
        RandomPlayer::performNextMove(board);
    }
}
