#include "human_player.hpp"
#include "board.hpp"

#include <iostream>

HumanPlayer::HumanPlayer(Color color) : Player(color)
{
}

void HumanPlayer::performNextMove(Board &board)
{
	std::vector<int> input;
	do {
		input = getUserInput();
		if (board[input[0]][input[1]] == Field::EMPTY)
			break;
        std::cout << "Feld bereits belegt!" << std::endl;
	} while (true);
	std::cout << "Alles klar." << std::endl;
	board[input[0]][input[1]] = asField(color);
}

std::vector<int> HumanPlayer::getUserInput()
{
	std::string input;
	do {
		std::cout
			<< "Wo wollen sie ihr Kreuz setzen? (Zaehlend von 0)"
			<< std::endl
			<< "Eingabeformat: <Zeile> <Spalte>, zum Beispiel '2 0':"
			<< std::endl;

		std::getline(std::cin, input);
		if (isValidInput(input)) {
            break;
		}
	} while (true);

    // subtract ASCII offset
	std::vector<int> result{ ((int)input[0]) - 48, ((int)input[2]) - 48 };
	return result;
}

bool HumanPlayer::isValidInput(std::string input)
{
	std::cout << input << std::endl;

	if (input.length() != 3) {
		std::cout << "Eingabe ist nicht 3 Zeichen lang (Leerzeichen eingeschlossen)!" << std::endl;
		return false;
	}

    if (input[1] != ' ') {
        std::cout << "Die Eingabe enthaelt kein Leerzeichen zwischen den Zahlen!" << std::endl;
        return false;
    }

	for (int i = 0; i < 3; i += 2) {
		int value = 0;
    	value = ((int)input[i]) - 48; // subtract ASCII offset
		if (value < 0 or value > 2) {
			std::cout << "Die Eingabe " << (int)input[i] - 48 << " liegt nicht zwischen 0 und 2" << std::endl;
			return false;
		}
	}
	return true;
}
