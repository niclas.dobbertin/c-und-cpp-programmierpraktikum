#include "random_player.hpp"
#include <cstdlib>

RandomPlayer::RandomPlayer(Color color) : Player(color) {}

void RandomPlayer::performNextMove(Board& board) {
    int x,y;
    do {
        x = rand()%3;
        y = rand()%3;
        if (board[x][y] == Field::EMPTY) {
            board[x][y] = asField(color);
            break;
        }
    } while (true);
}
