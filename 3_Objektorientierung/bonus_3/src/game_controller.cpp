#include "game_controller.hpp"
#include <cstdlib>
#include <iostream>

void GameController::play(Player& cross, Player& circle) {
  int selection = rand()%2;
  Player *current_player, *other_player, *temp;
  if (selection == 0){
      std::cout << "Der Mensch beginnt." << std::endl;
      current_player=&cross;
      other_player=&circle;
  } else {
      std::cout << "Der Computer beginnt." << std::endl;
      current_player=&circle;
      other_player=&cross;
  }

  do {
      current_player->performNextMove(board);
      temp = current_player;
      current_player = other_player;
      other_player = temp;
      std::cout << board << std::endl;
      if (board.whoWon().has_value()){
          GameStatus winner = *board.whoWon();
          if (winner == GameStatus::TIE){
              std::cout << "Unentschieden." << std::endl;
          } else if (winner == GameStatus::CROSS){
              std::cout << "Kreuz gewinnt." << std::endl;
          } else {
              std::cout << "Kreis gewinnt." << std::endl;
          }
          break;
      }
  } while (true);
}