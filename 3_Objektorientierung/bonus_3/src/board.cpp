#include "board.hpp"
#include "player.hpp"
#include <iostream>

GameStatus asGameStatus(Color color)
{
	switch (color) {
	case Color::CROSS:
		return GameStatus::CROSS;
	default:
		return GameStatus::CIRCLE;
	}
}

GameStatus asGameStatus(Field field)
{
	switch (field) {
	case Field::CROSS:
		return GameStatus::CROSS;
	case Field::CIRCLE:
		return GameStatus::CIRCLE;
	default:
		std::abort();
	}
}

Field asField(Color color)
{
	switch (color) {
	case Color::CROSS:
		return Field::CROSS;
	default:
		return Field::CIRCLE;
	}
}

Color enemyOf(Color color)
{
	switch (color) {
	case Color::CROSS:
		return Color::CIRCLE;
	default:
		return Color::CROSS;
	}
}

Board::Board()
	: fields(std::vector<std::vector<Field> >(
		  3, std::vector<Field>(3, Field::EMPTY)))
{
}

std::optional<GameStatus> Board::whoWon() const
{
	bool has_empty_field = false;
	for (size_t i = 0; i < 3; i++) {
		//Zeilen
		if (fields[i][0] != Field::EMPTY and
		    fields[i][0] == fields[i][1] and
		    fields[i][1] == fields[i][2]) {
			return { asGameStatus(fields[i][0]) };
		}
		//Spalten
		if (fields[0][i] != Field::EMPTY and
		    fields[0][i] == fields[1][i] and
		    fields[1][i] == fields[2][i]) {
            return { asGameStatus(fields[0][i]) };
		}
		for (size_t j = 0; j < 3; j++) {
			if (fields[i][j] == Field::EMPTY) {
				has_empty_field = true;
			}
		}
	}
	//Diagonale 1
	if (fields[0][0] != Field::EMPTY and fields[0][0] == fields[1][1] and
	    fields[1][1] == fields[2][2]) {
		return { asGameStatus(fields[0][0]) };
	}
	//Diagonale 2
	if (fields[2][0] != Field::EMPTY and fields[2][0] == fields[1][1] and
	    fields[1][1] == fields[0][2]) {
		return { asGameStatus(fields[2][0]) };
	}

	//Gleichstand
	if (!has_empty_field) {
		return { GameStatus::TIE };
	}

	//Spiel noch nicht zu Ende
	return {};
}

char asString(Field field) {
	switch (field) {
	case Field::CROSS:
		return 'X';
	case Field::CIRCLE:
		return 'O';
    case Field::EMPTY:
        return ' ';
	default:
		std::abort();
	}
}

std::ostream &operator<<(std::ostream &os, const Board &board)
{
    for(int i=0; i<3; i++) {
        for(int j=0; j<3; j++){
            os << "| " << asString(board[i][j]) << ' ';
        }
        os << "|" << std::endl;
    }
	return os;
}
