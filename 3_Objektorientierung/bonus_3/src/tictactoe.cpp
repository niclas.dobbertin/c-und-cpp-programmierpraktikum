#include "normal_player.hpp"
#include "human_player.hpp"
#include "game_controller.hpp"

#include <iostream>
#include <climits>

char select_opponent(){
    char input;
    do {
        std::cout << "Gegner waehlen (1: random, 2: normal, 3: perfect)" << std::endl;
        std::cin >> input;
        if (input == '1' or input == '2' or input == '3')
            return input;
        std::cout << "Ungueltige Eingabe." << std::endl;
    } while(true);
}

bool decide_continue(){
    char end;
    do {
        std::cout << "Nochmal Spielen? (j: ja, n: nein)" << std::endl;
        std::cin >> end;
        if (end == 'j'){
            return true;
        } else if (end == 'n'){
            return false;
        } else {
            std::cout << "Ungueltige Eingabe." << std::endl;
        }
    } while (true);
}

int main() {
    HumanPlayer human(Color::CROSS);
    RandomPlayer random(Color::CIRCLE);
    NormalPlayer normal(Color::CIRCLE);
    PerfectPlayer perfect(Color::CIRCLE);
    char selection;

    srand((unsigned) time(nullptr)); // seed für random numbers über aktuelle Zeit setzen

    do {
        GameController cont;
        selection = select_opponent();
        std::cin.ignore(INT_MAX, '\n'); // flush std::cin buffer
        if (selection == '1') {
            cont.play(human, random);
        } else if (selection == '2'){
            cont.play(human, normal);
        } else {
            cont.play(human, perfect);
        }

    if (!decide_continue())
        break;
    } while (true);
}
