#include "StudentAssistant.hpp"
#include <iostream>

StudentAssistant::StudentAssistant(const std::string& name, const std::string& studentID, const std::string& supervisor):
    Person(name), Student("Fred", studentID), Employee("Brudi", supervisor){
    std::cout << "Studentischer Mitarbeiter erstellt." << std::endl;
}

StudentAssistant::~StudentAssistant(){
    std::cout << "Studentischer Mitarbeiter geloescht." << std::endl;
}

std::string StudentAssistant::getInfo() const{
    return name + " " + studentID + " " + supervisor;
}