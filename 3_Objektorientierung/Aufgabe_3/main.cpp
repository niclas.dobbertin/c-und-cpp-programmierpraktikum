#include <iostream>
#include "Person.hpp"
#include "Student.hpp"
#include "Employee.hpp"
#include "StudentAssistant.hpp"

void printPersonInfo(const Person* person){
    std::cout << person->getInfo() << std::endl;
}

int main() {
    Person testPerson("Tobias");
    std::cout << testPerson.getInfo() << std::endl;
    Student testStudent("Mark", "0123456789");
    std::cout << testStudent.getInfo() << std::endl;
    Employee testEmployee("Heinrich", "Tobias");
    std::cout << testEmployee.getInfo() << std::endl;
    StudentAssistant testStudentAssistant ("Lotte", "187", "Frieda");
    std::cout << testStudentAssistant.getInfo() << std::endl;
    printPersonInfo(&testStudentAssistant);
    return 0;
}
