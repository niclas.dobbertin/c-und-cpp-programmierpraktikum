#include "Employee.hpp"
#include "Person.hpp"
#include <string>
#include <iostream>

Employee::Employee(const std::string& name, const std::string& supervisor):
    Person(name), supervisor(supervisor){
    std::cout << "Mitarbeiter mit dem Vorgesetzten " << supervisor << " erstellt." << std::endl;
}

Employee::~Employee(){
    std::cout << "Mitarbeiter mit dem Vorgesetzten " << supervisor << " geloescht." << std::endl;
}

std::string Employee::getInfo() const {
    return name + " " + supervisor;
}
