#pragma once
#include <string>
#include "Person.hpp"

class Employee: public virtual Person{
public:
    Employee(const std::string& name, const std::string& supervisor);
    virtual ~Employee();
    virtual std::string getInfo() const;
protected:
    std::string supervisor;
};

