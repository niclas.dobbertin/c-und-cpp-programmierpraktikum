#include "Student.hpp"
#include <iostream>

Student::Student(const std::string& name, const std::string& studentID):
    Person(name), studentID(studentID){
    std::cout << "Student mit der ID " << studentID << " erstellt." << std::endl;
}

Student::~Student(){
    std::cout << "Student mit der ID " << studentID << " geloescht." << std::endl;
}

std::string Student::getInfo() const{
    return Person::getInfo() + " " + studentID;
}