#pragma once
#include "Student.hpp"
#include "Employee.hpp"

class StudentAssistant: public Student, public Employee {
public:
    StudentAssistant(const std::string& name, const std::string& studentID, const std::string& supervisor);
    virtual ~StudentAssistant();
    virtual std::string getInfo() const;
};

