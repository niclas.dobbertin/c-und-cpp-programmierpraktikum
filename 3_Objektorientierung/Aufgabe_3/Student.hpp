#pragma once
#include <string>
#include "Person.hpp"

class Student : public virtual Person { // public inheritance
public:
    Student(const std::string& name, const std::string& studentID); // init name and ID
    virtual ~Student(); // destructor
    virtual std::string getInfo() const; // Person::getInfo() - get name and studentID
protected:
    std::string studentID; // the student ID of the student
};

