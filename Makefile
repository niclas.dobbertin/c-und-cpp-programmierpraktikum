task:
	cmake --build build --parallel
test:
	cmake --build build --parallel --target all test -- ARGS=--output-on-failure
setup:
	cmake -B build -DCMAKE_BUILD_TYPE=Debug
