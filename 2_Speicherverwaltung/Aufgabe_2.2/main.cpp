#include <iostream>
#include <cstddef>
#include <string>

using namespace std;

void taskA() {
    int ten[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    for (int i : ten) cout << i << " "; cout << endl;
}

void taskB() {
    int seven[7] = {1, 2, 3, 4, 5, 6, 7};
    int* pSeven = seven;
    *pSeven = *pSeven + 1;
    *(pSeven + 3) = seven[4] + *pSeven;
    *(pSeven + 1) = *pSeven + 2;
    seven[2] = *(pSeven + 5);
    *(pSeven + 6) = *(pSeven + 4) * *(pSeven);
    *(pSeven++) = *pSeven + *pSeven++;  // undefined behaviour!
    for (int i : seven) cout << i << " "; cout << endl;
}

void printElements(const int* const array, size_t size) {
    for (const int* p = array; p < array + size; p++) cout << *p << " "; cout << endl;  // using pointer
}

void printElements(const int* const begin, const int* const end) {
    for (const int* p = begin; p != end; p++) cout << *p << " "; cout << endl;  // using end marker (subarray possible)
}

void taskCDEF() {
    int five[] = {1, 2, 3, 4, 5};
    printElements(five, sizeof(five) / sizeof(five[0]));
    printElements(five + 1, five + 4);
}

int* readElements(size_t& size) {
    cout << "Größe: "; cin >> size;  // size can be a variable
    int* array = new int[size];
    for (int i = 0; i < size; i++) cin >> array[i];
    return array;
}

void taskG() {
    size_t size;
    int* pArr = readElements(size);
    printElements(pArr, size);
    delete[] pArr;  // notice the []!
    pArr = nullptr;
}

int* readElements(char** argv) {
    size_t size = stoi(argv[1]);
    int* array = new int[size];
    for (int i = 0; i < size; i++) cin >> array[i];
    return array;
}

void taskH(int argc, char** argv) {
    cout << "Programmname: " << argv[0] << endl;
    if (argc == 1) {cout << "Missing size Parameter!" << endl; return;}

    int* pArr = readElements(argv);
    printElements(pArr, stoi(argv[1]));
    delete[] pArr;
    pArr = nullptr;
}

void taskI() {
    int M[4][4] = {1, 2, 3, 4,
                   5, 6, 7, 8,
                   9, 10, 11, 12,
                   13, 14, 15, 16};
    int* ptrE = *M;
    cout << "M[0][1] = " << *(ptrE + 1) << endl;
    cout << "M[1][0] = " << *(ptrE + 4) << endl;
    cout << "M[2][1] = " << *(ptrE + 4*2 + 1) << endl;
}

void mmulC(int* m, int c) {
    for (int i = 0; i < 4; i++) for (int j = 0; j < 4; j++)
        *(m + 4*i + j) *= c;
}

void printMatrix(int m[4][4]) {
    for (int i = 0; i < 4; i++) {
        cout << "[";
        for (int j = 0; j < 4; j++)
            cout << " " << m[i][j];
        cout << " ]" << endl;
    }
}

void taskJ() {
    int I[4][4];
    for (int i = 0; i < 4; i++) for (int j = 0; j < 4; j++)
        I[i][j] = i == j ? 1 : 0;

    mmulC(*I, 2);
    printMatrix(I);
}

void taskK() {
    string str = "Hello";
    cout << "size: " << str.size() << ", length: " << str.length() << endl;
    cout << "append: " << str.append(" World!") << endl;
    cout << "replace: " << str.replace(5, 0, ", dear") << endl;
}

void drehen(std::string& s) {
    string reversed = s;
    for (int i = 0; i < reversed.length() / 2; i++) {
        size_t j = reversed.length() - 1 - i;
        char temp = reversed[i];
        reversed[i] = reversed[j];
        reversed[j] = temp;
    }
    s = reversed;
}

void decoder(std::string b) {
    if (b.length() % 2 == 1)
        b = b.substr(0, b.length() - 1);

    size_t n = b.length();
    for (size_t i = 0; i < n; i += 2) {
        char c = b[n-2-i] - (b[n-1-i] - 48);
        cout << c;
    }
    cout << endl;
}

void taskLM() {
    string s = "w3r6l7x5v3j5{6n2q9d1u2{7n0n9#3k6d0u6E2";
    decoder(s);
}

int main(int argc, char** argv) {
    // taskA();
    // taskB();
    // taskCDEF();
    // taskG();
    // taskH(argc, argv);
    // taskI();
    // taskJ();
    // taskK();
    // taskLM();
}

/* SECOND VERSION
#include <iostream>
#include <cstddef>
#include <string>

void printArray(const int* const array, size_t size){
    std::cout << "[ ";
    for (int i = 0; i <size; i++){
        if (i == size-1) {
            std::cout << array[i];
        } else {
            std::cout << array[i] << ", ";
        }

    }
    std::cout << " ]" << std::endl;
}

void printArray1(const int* const array, size_t size){
    //Processes full array
    std::cout << "[ ";
    for (const int* p = array; p != array + size; p++){
        std::cout << *p << ", ";
    }
    std::cout << " ]" << std::endl;
}

void printArray2(const int* const begin, const int* const end) {
    //Accepts subarrays e.g. index 2-8 of an array
    std::cout << "[ ";
    for (const int *p = begin; p != end; ++p) {
        std::cout << *p << ", ";
    }
    std::cout << " ]" << std::endl;
}

int* createCustomArray(const int* arrSize){
    int* pArr = new int[*arrSize];
    for (int i = 0; i < *arrSize; i++){
        std::cout << "Element " << i << " eingeben: ";
        std::cin >> pArr[i];
    }
    return pArr;
}

void mmulC(int* m, int c) {
    for (int k = 0; k<16; k++){
        *(m+k) = *(m+k) * c;
    }
}

void rotateString(std::string& s){
    std::string copy = s;
    for (int i = 0; i<copy.length(); i++){
        s[i] = copy[copy.length()-i-1];
    }
}

void decoder(std::string &s){
    char c;
    int j;
    std::string result;
    for (int i = 0; i < s.length()/2; i++){
        c = s[i];
        j = int(s[i+1]);
        std::cout << char(c-j) << std::endl;
    }
}

int main(int argc, char** argv) {
    std::cout << "Aufgabe 2.2 a)" << std::endl;
    int testArr[10];
    for (int i = 1; i <=10; i++){
        testArr[i-1] = i;
        std::cout << testArr[i-1] << std::endl;
    }

    std::cout << std::endl << "Aufgabe 2.2 b)" << std::endl;
    int arr[7];
    for (int i = 1; i <=7; i++){
        arr[i-1] = i;
    }
    int * pArr = arr;
    std::cout << arr[0] << std::endl;
    std::cout << *pArr << std::endl;
    std::cout << arr << std::endl;
    std::cout << pArr << std::endl;

    *pArr = *pArr + 1;
    *(pArr + 3) = arr[4] + *pArr;
    *(pArr + 1) = *pArr + 2;
    arr[2] = *(pArr + 5);
    *(pArr + 6) = *(pArr + 4) * *(pArr);
    *(pArr++) = *pArr + *pArr++;
    // expected result: [2, 4, 6, 7, 5, 6, 10]

    for (int i = 0; i <7; i++){
        std::cout << arr[i] << std::endl;
    }

    std::cout << std::endl << "Aufgabe 2.2 c)" << std::endl;
    printArray(testArr, sizeof(testArr) / sizeof(testArr[0]));

    std::cout << std::endl << "Aufgabe 2.2 d)" << std::endl;
    printArray1(testArr, sizeof(testArr) / sizeof(testArr[0]));

    std::cout << std::endl << "Aufgabe 2.2 e)" << std::endl;
    printArray2(testArr, testArr+(sizeof(testArr) / sizeof(testArr[0])));

    std::cout << std::endl << "Aufgabe 2.2 f)" << std::endl;
    printArray2(testArr+2, testArr+8);

    // std::cout << std::endl << "Aufgabe 2.2 h)" << std::endl;
    // std::cout << argv[0] << std::endl;
    // int* customArr;
    // int customSize = std::stoi(argv[1]);
    // customArr = createCustomArray(&customSize);
    // std::cout << "Ergebnis:";
    // printArray(customArr, customSize);
    // delete[] customArr;
    // customArr = nullptr;

    std::cout << std::endl << "Aufgabe 2.2 i)" << std::endl;
    int M[4][4] = { 1, 2 , 3 ,4 ,5 ,6 ,7 ,8 ,9 , 10, 11, 12, 13 , 14 ,15 ,16};
    int* ptrE = *M;
    std::cout << "M[0][0]: " << *ptrE << std::endl;
    std::cout << "M[0][1]: " << *(ptrE+1) << std::endl;
    std::cout << "M[1][0]: " << *(ptrE+4) << std::endl;
    //auf M[n][m] kann man durch ptrE + 4*n + m zugreifen.

    std::cout << std::endl << "Aufgabe 2.2 j)" << std::endl;
    int I[4][4];
    for (int n = 0; n<4; n++ ){
        for (int m = 0; m<4; m++){
            if (m==n){
                I[n][m]=1;
            } else {
                I[n][m]=0;
            }
        }
    }
    printArray(*I, 16);

    std::cout << std::endl << "Aufgabe 2.2 k)" << std::endl;
    mmulC(*I, 3);
    printArray(*I, 16);

    std::cout << std::endl << "Aufgabe 2.2 l)" << std::endl;
    std::string s = "Hallo";
    std::cout << "Laenge vom Wort '"<< s <<"': "<< s.length() << std::endl;
    std::cout << "Groesse vom Wort '"<< s <<"': "<< s.size() << std::endl;
    s.append(" du Fisch");
    std::cout << s << std::endl;
    s.replace(9, 5, "Otto");
    std::cout << s << std::endl;

    std::cout << std::endl << "Aufgabe 2.2 m)" << std::endl;
    std::string y = "Hallo Opa";
    rotateString(y);
    std::cout << y << std::endl;

    std::cout << std::endl << "Aufgabe 2.2 n)" << std::endl;
    std::string x = "w3r6l7x5v3j5{6n2q9d1u2{7n0n9#3k6d0u6E2";
    decoder(x);

    return 0;
}
*/
