#include "map.hpp"
#include <string>
#include <vector>
#include <stdexcept>
#include <algorithm>

cppp::Map::Map(const std::size_t size) : size(size) {
    arr = new MapBucket[size];
}

cppp::Map::~Map() {
    delete[] arr;
}

void cppp::Map::insert(const std::string& key, const std::vector<Item>& order) {
    std::size_t pos = calcHash(key); //Hash Wert berechnen
    // Wenn der Bucket an der berechneten Position leer ist, dann wird dort direkt ein neues MapElement eingefügt
    if (arr[pos].empty()){
        MapElement insertion;
        insertion.key = key;
        insertion.value = order;
        arr[pos].push_back(insertion);
    } else { // Fallunterscheidung, falls der Bucket an der berechneten Stelle nicht leer ist.
        bool alreadyPresent = false;
        // Loop über Bucket, um herauszufinden, ob der Schlüssel bereits im Bucket vorhanden ist
        for (std::size_t i=0; i<arr[pos].size(); i++){
            if (arr[pos][i].key == key){
                alreadyPresent = true;
                // Wenn der Schlüssel bereits vorhanden ist, dann wird lediglich der Wert überschrieben
                arr[pos][i].value = order;
                break;
            }
        }
        // Wenn der Schlüssel nicht vorhanden ist, dann wird ein neues MapElement im Bucket angelegt
        if (!alreadyPresent){
            MapElement insertion;
            insertion.key = key;
            insertion.value = order;
            arr[pos].push_back(insertion);
        }
    }
}

std::vector<cppp::Item> cppp::Map::get(const std::string& key) {
    std::vector<cppp::Item> result;
    std::size_t pos = calcHash(key); //Hash Wert berechnen
    bool nothingFound = true;
    for (std::size_t i = 0; i < arr[pos].size(); i++) {
        if (arr[pos][i].key == key) {
            // Wenn der Schlüssel vorkommt, wird der Wert des MapElements an dieser Stelle zurückgegeben
            result = arr[pos][i].value;
            nothingFound = false;
            break;
        }
    }
    if (nothingFound){
        std::string message = "Der gesuchte Wert wurde nicht im Hash Table gefunden";
        throw std::invalid_argument(message);
    }
    return result;
}

void cppp::Map::remove(const std::string &key) {
    std::size_t pos = calcHash(key); //Hash Wert berechnen
    // Suche nach dem Schlüssel
    for (std::size_t i = 0; i < arr[pos].size(); i++) {
        if (arr[pos][i].key == key) {
            // Wenn der Schlüssel vorkommt, wird das MapElement an dieser Stelle gelöscht
            arr[pos].erase(arr[pos].begin() + i);
            break;
        }
    }
}

std::size_t cppp::Map::calcHash(const std::string& key) {
    std::size_t sum = 0;
    for (std::size_t i=0; i<key.length(); i++){
        sum += int(key[i]);
    }
    return sum % size;
}
