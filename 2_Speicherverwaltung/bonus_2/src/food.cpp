#include "food.hpp"
#include <fstream>
#include <sstream>
#include <string>
#include <stdexcept>
#include <iostream>

Food::Food(int bestellnummer, std::string bezeichnung, float preis) : bestellnr(bestellnummer), bezeichnung(bezeichnung), preis(preis) {}

std::string Food::getBezeichnung() const {
    return bezeichnung;
}

float Food::getPreis() const {
    return preis;
}

int Food::getBestellnummer() const {
    return bestellnr;
}

void speichern(const std::string& dateiname, const std::vector<Food>& speisen) {
    // Remove potential leading backslash
    std::string path = dateiname;
    if (path.front() == '\\') path.erase(0, 1);

    // Open CSV file for writing
    std::fstream s(path, std::fstream::out | std::fstream::trunc);
    if (!s.is_open()) throw std::runtime_error("Fehler beim Oeffnen der Datei waehrend des Speichervorgangs");

    // Write data to CSV file
    for (const Food& f : speisen)
        s << f.getBestellnummer() << ";" << f.getBezeichnung() << ";" << f.getPreis() << "\n";

    s.close();
}

void laden(const std::string& dateiname, std::vector<Food>& speisen) {
    // Remove potential leading backslash
    std::string path = dateiname;
    if (path.front() == '\\') path.erase(0, 1);

    // Open CSV file for reading
    std::fstream s(path, std::fstream::in);
    if (!s.is_open())
        throw std::runtime_error("Fehler beim Oeffnen der Datei waehrend des Ladevorgangs");

    // Read file line by line
    std::vector<std::string> lines;
    std::string line;
    while (std::getline(s, line, '\n'))
        lines.push_back(line);

    // Iterate over all lines
    std::ostringstream oss;  // Stream for potential exception
    for (int i = 0; i < lines.size(); i++){
        std::vector<std::string> cells;
        std::string cell;
        std::stringstream lineStream(lines[i]);  // Convert to stringstream for usage in getline()

        // Extract cells of current line
        while (std::getline(lineStream, cell, ';')) {
            if (cell.empty()) {
                oss << "Zeile " << i + 1 << " enthaelt eine leere Zelle";
                throw std::runtime_error(oss.str());
            }
            cells.push_back(cell);
        }

        // Handle wrong format used in CSV
        if (cells.size() > 3) {
            oss << "Zeile "  << i+1 << " enthaelt zu viele Zellen";
            throw std::runtime_error(oss.str());
        }
        if (cells.size() < 3) {
            oss << "Zeile "  << i+1 << " enthaelt zu wenige Zellen";
            throw std::runtime_error(oss.str());
        }

        // Insert as a new Food object
        Food f(std::stoi(cells[0]), cells[1], std::stof(cells[2]));
        speisen.push_back(f);  // https://en.cppreference.com/w/cpp/container/vector/push_back
    }

    s.close();
}
