#include <iostream>

using namespace std;

void printExperiments() {
    int intVal = 42;
    int* pIntVal = &intVal;
    cout << "Wert von IntVal " << intVal << endl;      // 42
    cout << "Wert von &IntVal " << &intVal << endl;    // 0xXXXX
    cout << "Wert von pIntVal " << pIntVal << endl;    // 0xXXXX
    cout << "Wert von *pIntVal " << *pIntVal << endl;  // 42
    cout << "Wert von &pIntVal " << &pIntVal << endl;  // 0xYYYY
}

void swapPointers(int** p1, int** p2) {
    int* temp = *p1;
    *p1 = *p2; *p2 = temp;
}

void square(int a) {a = a * a;}  // call by value -> leaves input unchanged!

void square2(int* a) {*a = *a * *a;}  // call by pointer

void square3(int& a) {a = a * a;}  // call by reference

void differingMethods() {
    int intVal = 5;
    int* pIntVal = &intVal;

    square(intVal);
    cout << intVal << endl;  // 5
    square2(pIntVal);
    cout << *pIntVal << endl;  // 25
    square3(intVal);
    cout << intVal << endl;  // 625
}

void swapPointer(int* a, int* b) {
    int temp = *a;
    *a = *b; *b = temp;
}

void swapReference(int& a, int& b) {
    int temp = a;
    a = b; b = temp;
}

void foo(int& i) {
    int i2 = i;
    int& i3 = i;

    cout << "i = " << i << endl;
    cout << "i2 = " << i2 << endl;
    cout << "i3 = " << i3 << endl;
    cout << "&i = " << &i << endl;
    cout << "&i2 = " << &i2 << endl;
    cout << "&i3 = " << &i3 << endl;
}

void callFoo() {
    int var = 42;
    cout << "&var = " << &var << endl;
    foo(var);
}

int main() {
    // printExperiments();
    // differingMethods();
    // callFoo();
}

/* SECOND VERSION
#include <iostream>

void printSwap(int a, int b, bool beforeSwap){
    if (beforeSwap == true){
        std::cout << "Vor dem Tausch: " << std::endl;
    } else {
        std::cout << "Nach dem Tausch: " << std::endl;
    }
    std::cout << "Wert 1: " << a << std::endl;
    std::cout << "Wert 2: " << b << std::endl << std::endl;
}

void swapPointers(int* p1, int* p2){
    std::cout << "Swap Pointers" << std::endl;
    printSwap(*p1, *p2, true);
    int* temp = p1;
    p1 = p2;
    p2 = temp;
    printSwap(*p1, *p2, false);
}

void swapIntByReference(int& p1, int& p2){
    int temp = p1;
    p1 = p2;
    p2 = temp;
}

void swapIntByPointer(int* p1, int* p2){
    int temp = *p1;
    *p1 = *p2;
    *p2 = temp;
}


void printLine(int length){
    for (int i = 0; i < length; i++){
        std::cout << "-";
    }
    std::cout << std::endl;
}

void foo(int& i) {
    int i2 = i;
    int& i3 = i;
    std::cout << "i = " << i << std::endl;
    std::cout << "i2 = " << i2 << std::endl;
    std::cout << "i3 = " << i3 << std::endl;
    std::cout << "&i = " << &i << std::endl;
    std::cout << "&i2 = " << &i2 << std::endl;
    std::cout << "&i3 = " << &i3 << std::endl;
}

int main() {
    printLine(30);
    int intVal = 42;
    int* pIntVal = &intVal;
    std::cout << "Wert von IntVal " << intVal << std::endl;
    std::cout << "Wert von &IntVal " << &intVal << std::endl;
    std::cout << "Wert von pIntVal " << pIntVal << std::endl;
    std::cout << "Wert von *pIntVal " << *pIntVal << std::endl;
    std::cout << "Wert von &pIntVal " << &pIntVal << std::endl;

    printLine(30);
    int a = 1, b = 2;
    swapPointers(&a, &b);

    printLine(30);
    a = 5, b = 10;
    std::cout << "Swap Int by Reference" << std::endl;
    printSwap(a, b, true);
    swapIntByReference(a, b);
    printSwap(a, b, false);

    printLine(30);
    a = 20, b = 50;
    std::cout << "Swap Int by Pointer" << std::endl;
    printSwap(a, b, true);
    swapIntByPointer(&a, &b);
    printSwap(a, b, false);

    printLine(30);
    int var = 42;
    std::cout << "&var = " << &var << std::endl;
    foo(var);


    return 0;
}
*/
