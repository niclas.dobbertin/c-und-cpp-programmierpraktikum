#include <iostream>
#include "List.hpp"

int main() {
    List l;
    for (int i=0; i<10; i++){
        l.appendElement(i);
    }
    std::cout << l << std::endl;
    std::cout << l.getNthElement(3) << std::endl;
    l.deleteAt(3);
    std::cout << l << std::endl;
    l.insertElementAt(19, 6);
    std::cout << l << std::endl;
    std::cout << l.getFirst() << std::endl;
    std::cout << l.getLast() << std::endl;
    List l2 = l;
    std::cout << l << std::endl;
    std::cout << l2 << std::endl;
    std::cout << &l << std::endl;
    std::cout << &l2 << std::endl;
    std::cout << l2.getSize() << std::endl;

    for (ListIterator iter = l.begin(); iter != l.end(); iter++) {
        std::cout << *iter << std::endl;
    }
}
