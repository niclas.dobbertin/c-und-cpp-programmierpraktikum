#include "ListIterator.hpp"
#include "List.hpp"
#include "ListItem.hpp"

ListIterator::ListIterator(List* list, ListItem* item): list(list), item(item){};

int& ListIterator::operator*(){
    return item->getContent();
};

bool ListIterator::operator!=(const ListIterator& other) const{
    return this != &other;
};

// Prefix ++ -> increment iterator and return it
ListIterator& ListIterator::operator++() {
    if (item == NULL) {
        item = list->first; // set item to first item of list
    }
    else {
        item = item->getNext(); // set item to next item of current item
    }
    return *this; // return itself
}
// Postfix ++ -> return iterator to current item and increment this iterator
ListIterator ListIterator::operator++(int) {
    ListIterator iter(list, item); // Store current iterator
    if (item == NULL) {
        item = list->first; // set item to first item of list
    }
    else {
        item = item->getNext(); // set item to next item of current item
    }
}

// Prefix -- -> decrement iterator and return it
ListIterator& ListIterator::operator--() {
    if (item == NULL) {
        item = nullptr; // set item to first item of list
    }
    else {
        item = item->getPrevious(); // set item to next item of current item
    }
    return *this; // return itself
}
// Postfix -- -> return iterator to current item and decrement this iterator
ListIterator ListIterator::operator--(int) {
    ListIterator iter(list, item); // Store current iterator
    if (item == NULL) {
        item = nullptr; // set item to first item of list
    }
    else {
        item = item->getPrevious(); // set item to next item of current item
    }
}