#include "List.hpp"
#include "ListItem.hpp"
#include "ListIterator.hpp"

List::List(): first(nullptr), last(nullptr), currentSize(0){}

List::~List() {
    while (currentSize > 0) {
        deleteFirst();
    }
}

List::List(const List& other): first(nullptr), last(nullptr), currentSize(0){
    ListItem* element = other.first;
    for (int i = 0; i<other.currentSize; i++){
        appendElement(element->getContent());
        element = element->getNext();
    }
}

void List::appendElement(int i){
    ListItem* s = new ListItem(last, nullptr, i);
    last = s;
    if (first == nullptr){
        first = s;
    }
    currentSize ++;
}

void List::prependElement(int i){
    ListItem* s = new ListItem(nullptr, first, i);
    first = s;
    if (last == nullptr){
        last = s;
    }
    currentSize ++;
}

void List::insertElementAt(int i, size_t pos){
    if (pos == 0) {
        prependElement(i);
    }
    else if (pos >= getSize()) {
        appendElement(i);
    }
    else {
        ListItem* p = first;
        // iterate over elements
        while (pos-- > 0) {
            p = p->getNext();
        }
        new ListItem(p->getPrevious(), p, i);
        ++currentSize;
    }
}

size_t List::getSize() const{
    return currentSize;
}

int& List::getNthElement(size_t n){
    ListItem* element = first;
    for (int i= 0; i<n; i++){
        element = element->getNext();
    }
    return element->getContent();
}

int& List::getFirst(){
    return first->getContent();
}

int& List::getLast(){
    return last->getContent();
}

int List::deleteFirst(){
    if(first) {
        ListItem *oldFirst = first;
        ListItem *newFirst = oldFirst->getNext();
        if (newFirst== nullptr){
            last= nullptr;
        }
        int s = first->getContent();
        delete first;
        first = newFirst;
        currentSize--;
        return s;
    }
    return 0;
}

int List::deleteLast(){
    if(last) {
        ListItem *oldLast = last;
        ListItem *newLast = oldLast->getPrevious();
        if (newLast== nullptr){
            first= nullptr;
        }
        int s = last->getContent();
        delete last;
        last = newLast;
        currentSize--;
        return s;
    }
    return 0;
}

int List::deleteAt(size_t pos){
    if (pos == 0) {
        return deleteFirst();
    }
    else if (pos >= currentSize - 1) {
        return deleteLast();
    }
    else {
        size_t index = pos;
        ListItem* p = first;

        // iterate over elements
        while (index-- > 0) {
            p = p->getNext();
        }
        int content = p->getContent();
        delete p;
        --currentSize;
        return content;
    }
}

std::ostream& operator<<(std::ostream& stream, List& list){
    //stream << "Groesse:" << list.getSize();
    stream <<  "[ ";
    for (int i = 0; i < list.getSize(); i++){
        if (i == list.getSize()-1) {
            stream << list.getNthElement(i);
        }else{
            stream << list.getNthElement(i) << ", ";
        }
    }
    stream <<  " ]";
    return stream;
}

ListIterator List::begin() {
    return ListIterator(this, first);
}

ListIterator List::end() {
    return ListIterator(this, nullptr);
}