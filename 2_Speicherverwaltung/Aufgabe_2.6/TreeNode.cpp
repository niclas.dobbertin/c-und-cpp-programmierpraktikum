#include "TreeNode.hpp"
#include <iostream>

TreeNode::TreeNode(int content, TreeNodePtr left, TreeNodePtr right):
content(content), leftChild(left), rightChild(right){
    std::cout << "Treenode with content " << content << " created" << std::endl;
}

TreeNode::~TreeNode(){
    std::cout << "Treenode with content " << content << " destroyed" << std::endl;
}

TreeNodePtr TreeNode::createNode(int content, TreeNodePtr left /* = TreeNodePtr() */, TreeNodePtr right /* = TreeNodePtr() */){
    TreeNodePtr node(new TreeNode(content, left, right));
    if (left) {
        left->setParent(node); // set parent node
    }
    if (right) {
        right->setParent(node); // set parent node
    }
    return node;
}

void TreeNode::setParent(const TreeNodePtr& p){
    parent = p;
}