#include <iostream>
#include "TreeNode.hpp"

int main() {
    TreeNodePtr node = TreeNode::createNode(1, TreeNode::createNode(2), TreeNode::createNode(3));
    node = TreeNode::createNode(7, TreeNode::createNode(8), TreeNode::createNode(9));
    std::cout << "Ende" << std::endl;
}
