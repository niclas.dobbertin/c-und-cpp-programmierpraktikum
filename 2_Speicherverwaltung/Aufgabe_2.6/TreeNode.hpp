#pragma once
#include <memory>

class TreeNode;
typedef std::shared_ptr<TreeNode> TreeNodePtr; // typedef for better readability
typedef std::weak_ptr<TreeNode> TreeNodeWeakPtr;

class TreeNode {
public:
    static TreeNodePtr createNode(int content, TreeNodePtr left = TreeNodePtr(), TreeNodePtr right = TreeNodePtr()); //create a new tree node and make it shared
    ~TreeNode();
    void setParent(const TreeNodePtr& p); // set parent of this node
private:
    TreeNode(int content, TreeNodePtr left, TreeNodePtr right); // create a tree node
    TreeNodePtr leftChild, rightChild; // left and right child
    int content; // node content
    TreeNodeWeakPtr parent; // parent node
};
