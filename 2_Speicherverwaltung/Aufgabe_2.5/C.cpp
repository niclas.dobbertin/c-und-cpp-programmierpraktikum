#include "C.hpp"


C::C(){
    std::cout << "Class C created" << std::endl;
}

C::C(const C &other){
    std::cout << "Class C copied" << std::endl;
}

C::~C(){
    std::cout << "Class C deleted" << std::endl;
}

