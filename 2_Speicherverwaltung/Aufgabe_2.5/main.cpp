#include <iostream>
#include "C.hpp"

int main() {
    // multiple catch blocks
    try {
        throw C();
    }
    catch (const C& c) {
        std::cout << "Cought C by reference" << std::endl;
    }
    catch (C c) {
        std::cout << "Cought C by value" << std::endl;
    }

}
