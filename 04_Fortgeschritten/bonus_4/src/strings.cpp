#include "strings.hpp"
#include <algorithm>
#include <cctype>
#include <iterator>

size_t countAbc(const std::string &input)
{
	size_t count = std::count_if(input.begin(), input.end(), [](char c) {
		return (('A' <= c && 'Z' >= c) || ('a' <= c && 'z' >= c));
	});
	return count;
}

std::map<char, size_t> countIndividual(const std::string &input)
{
	std::map<char, size_t> char_counts;
	for (char c : input) {
		if ('a' <= std::tolower(c) && 'z' >= std::tolower(c))
			char_counts[std::tolower(c)]++;
	}
	return char_counts;
}

SymbolCounter::SymbolCounter(std::initializer_list<char> lst) : my_chars(lst)
{
}

bool SymbolCounter::operator()(char c) const
{
	size_t count = std::count(my_chars.begin(), my_chars.end(), c);
	return (count > 0); // explicit since bool is return type
}

std::list<char> usedSymbols(const std::string &input)
{
	std::list<char> input_copy;
	std::transform(input.begin(), input.end(),
		       std::back_inserter(input_copy),
		       [](auto var) { return std::tolower(var); });
	input_copy.sort();
    auto last = std::unique(input_copy.begin(), input_copy.end());
    input_copy.erase(last, input_copy.end());

	return input_copy;
}
