#include <cctype>
#include <cstddef>
#include "strings.hpp"
#include "numerik.hpp"
#include <initializer_list>
#include <iostream>
#include <iomanip>
#include <iterator>
#include <ostream>
#include <string>
#include <algorithm>
#include <cmath>

using namespace std;

// Helper function for task 4.2
// Need to use because 'newton()' signature is mistakenly taking a float function,
// however the lambda function evaluates to double.
// To prevent tests to fail, we implement the helper function.
float fd(float x) {
    return 3 * std::pow(x, 2) + 8 * x - 7;
}

int main(int argc, char** argv) {
    // Test String
    std::string str = "Hello World!";

    cout << "4.1a) Alle Buchstaben zählen: " << countAbc(str) << "\n" << endl;

    cout << "4.1b) Einzelne Buchstaben zählen:" << endl;
    for (const auto& n : countIndividual(str))  // iterate over all letters in map
        cout << "- " << n.first << " = " << n.second << endl;
    cout << endl;

    cout << "4.1c) Symbole zählen: ";
    SymbolCounter sc({'a', 'o', '!'});
    cout << count_if(str.begin(), str.end(), sc) << "\n" << endl;

    cout << "4.1d) Vielfalt der Symbole: { ";
    for (char c : usedSymbols(str)) cout << c << " ";
    cout << "}\n" << endl;

    cout << "4.2a) Newton-Verfahren:" << endl;
    auto fx = [](double x) { return std::pow(x, 3) + 4 * std::pow(x, 2) - 7 * x + 12; };
    auto fderiv = [](float x){ return 3 * std::pow(x, 2) + 8 * x - 7; };
    double solution = newton(fx, fd, 0, 1000);  // using helper function fd, see above as to why
    cout << "- solution: x = " << setprecision(10) << solution << "\n" << endl;

    cout << "4.2b) Templates:" << endl;
    float newton_float = newtonTemp(fxTemp, fderivTemp, (float)0, 1000);
    double newton_double = newtonTemp(fxTemp, fderivTemp, (double)0, 1000);
    unsigned int newton_uint = newtonTemp(fxTemp, fderivTemp, (unsigned int)0, 1000);
    int newton_int = newtonTemp(fxTemp, fderivTemp, 0, 1000);

    cout << "- newton float: x = " << std::setprecision(10) << newton_float << endl;
    cout << "- newton double: x = "<< std::setprecision(10) << newton_double << endl;
    cout << "- newton uint: x = " << newton_uint << endl;
    cout << "- newton int: x = " << newton_int << endl;
}
