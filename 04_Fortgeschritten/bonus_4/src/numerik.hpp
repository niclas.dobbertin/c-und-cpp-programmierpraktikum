#pragma once
#include <cstddef>
#include <functional>
#include <cmath>

double newton(double (*fx)(double), float (*fderiv)(float), double x0, size_t n);

template <typename T>
T newtonTemp(T (*fx)(T), T (*fderiv)(T), T x0, size_t n) {
    T current;
    T previous = x0;
    for (size_t i = 0; i <= n; i++) {
        current = previous - (fx(previous) / fderiv(previous));
    }

    return current;
}

template <typename T>
T fxTemp(T x) {
    return x * x * x + 4 * x * x - 7 * x + 12;
}

template <typename T>
T fderivTemp(T x) {
    return 3 * x * x + 8 * x - 7;
}
