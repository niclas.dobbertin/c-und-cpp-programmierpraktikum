#include "display.h"

#include "gfx.h"
#include "glcdfont.h"
#include <stdio.h>

static int16_t cursorX, cursorY;
static uint16_t textColor;
static uint8_t textSize;
static int textBackground;



uint16_t color565(const uint8_t r, const uint8_t g, const uint8_t b) {
    uint8_t hiR = r & (0b11111000);
    uint8_t hiRShifted = (hiR >> 3);
    
    uint8_t hiG = g & (0b11111100);
    uint8_t hiGShifted = (hiG >> 2);
    
    uint8_t hiB = b & (0b11111000);
    uint8_t hiBShifted = (hiB >> 3);
    
    uint16_t result = (hiRShifted << 11) | (hiGShifted << 5) | hiBShifted;
    
    return result;
}

void printPattern(const uint16_t backgroundColor, const uint16_t foregroundColor) {
    cppp_fillScreen(backgroundColor);
    int block_size = 32;
    
    for (int x = 0; x < 480; x +=block_size) {
       for (int y = 0; y < 320; y +=block_size) {
          if ((x / block_size) % 2 !=  (y / block_size) % 2) {
            cppp_fillRect(x, y, block_size, block_size, foregroundColor);
          }
       }
    }
}

void initCursor() {
    setCursor(0, 0);
    setTextColor(WHITE);
    setBackgroundColor(BLACK);
    setTextSize(2);
}

void setCursor(const int16_t x, int16_t y) {
    cursorX = x;
    cursorY = y;
}

void setTextColor(const uint16_t c) {
    // For 'transparent' background, we'll set the bg to the same as the rest of the display
    textColor = c;
}

void setTextSize(const uint8_t s) {
    textSize = s;
}

void setBackgroundColor(const int bg) {
    textBackground = bg;
}

void drawChar(const int x, const int y, const char c, const int color, const int bg, const char size) {
    // If (x,y) is not inside the display return
    if (x > 480 || y > 320 || (x + 5*size) > 480 || (y + 7*size) > 320 || x < 0 || y < 0) return;
    char i, j;
    for (i = 0; i < 5; i++) { // Draw in x-direction
        char line = font[c*5 + i];            // Line (i,j) ... (i,j+7)
        // save the i.x-line from (i,j) to (i,j+7) in the char line

        for (j = 0; j < 8; j++, line <<= 1) { // draw in y-direction
           if ((line & 0b10000000)) {                  // Check whether the j'th bit in line is set
             cppp_fillRect(x+i*size, y+j*size, size, size, color);
           }else {
            cppp_fillRect(x+i*size, y+j*size, size, size, bg);
           }
        }
    }
    
    cppp_fillRect(x+i*size, y, size, 8*size, bg);
}

void writeAuto(const char c) {
    // if char c == '\n' then jump cursor to next line
    if (c == '\n') {
      cursorY += 8*textSize;
      cursorX = 0;
      if (cursorY >= 320)
        cursorY = 0;
    } else {
      
    // else
   // check for end of line and reset cursor. in the case of end of the display set cursorY to the starting point.
    
    // draw the char
    drawChar(cursorX, cursorY, c, textColor, textBackground, textSize);
    // move cursorX
    cursorX += 6*textSize;
    if (cursorX >= 480)
      cursorX = 0;
    
    }
    
    
}

void writeText(const char* text) {
    for (int i = 0; text[i] != '\0'; i++) {
     writeAuto(text[i]); 
    }
}

void writeTextln(const char* text) {
    writeText(text)  ;
     writeAuto('\n');
}

void writeNumberOnDisplay(const uint8_t* value) {
  char number[4];
  sprintf(number, "%d", *value);
  writeText(number);
}

void writeNumberOnDisplayRight(const uint8_t* value) {
    char number[4];
  sprintf(number, "%*d", 3, *value);
  writeText(number);
}

void write16BitNumberOnDisplay(const uint16_t* value, uint8_t mode) {
  char number[6];
  int pad_length;
  if (mode==1) {
    pad_length = 0;
  }else{
    pad_length = 5;
  }
  sprintf(number, "%*d", pad_length, *value);
  writeText(number);
}