#include "joystick.h"

#include <stdint.h>
#include "s6e2ccxj.h"
#include "analog.h"
#include "display.h"
#include "pins.h"


void controlLedsInit() {
    // Turn off analog Pins


    // Define Pins as outputs


    // Turn all LEDs down
  
  // Turn analog off for red led and initialize direction and value
    LED_RED_DDR |= (1 << LED_RED_PIN); // Configure blue LED pin as output.
    LED_RED_DOR |= (1 << LED_RED_PIN); // Turn LED off.
    // Turn analog off for green led and initialize direction and value
    LED_GREEN_DDR |= (1 << LED_GREEN_PIN); // Configure blue LED pin as output.
    LED_GREEN_DOR |= (1 << LED_GREEN_PIN); // Turn LED off.
    // Turn analog off for blue led and initialize direction and value
    LED_BLUE_DDR |= (1 << LED_BLUE_PIN); // Configure blue LED pin as output.
    LED_BLUE_DOR |= (1 << LED_BLUE_PIN); // Turn LED off.
}

void controlLeds() {
    // Get analog values
  uint8_t analog11;
  uint8_t analog12;
  uint8_t analog13;
  uint8_t analog16;
  uint8_t analog19;
  uint8_t analog23;
  uint8_t analog17;
  cppp_getAnalogValues(&analog11, &analog12, &analog13, &analog16, &analog17, &analog19, &analog23);
  

    // green => Pin104 PB2/A18    blue => Pin106 P18/A08     red => Pin108 P1A/A10
    // JS1X is analog16
   
 
    // left: JS1 X < 255 && >= 200   => green
    if (analog16 <= 255 && analog16>=200) {
      LED_GREEN_DOR &= ~(1 << LED_GREEN_PIN);
  }else {
     LED_GREEN_DOR |= (1 << LED_GREEN_PIN);
  }
    // middle: JS1 X < 200 && >= 180  => blue
  if (analog16 < 200 && analog16>=180) {
      LED_BLUE_DOR &= ~(1 << LED_BLUE_PIN);
  }else {
     LED_BLUE_DOR |= (1 << LED_BLUE_PIN);
  }
    // right: JS1 X <180 && >= 0      => red
 if (analog16 < 180 && analog16>=0) {
      LED_RED_DOR &= ~(1 << LED_RED_PIN);
  }else {
     LED_RED_DOR |= (1 << LED_RED_PIN);
  }
 
  if (analog19 <= 255 && analog19>=200) {
      LED_GREEN_DOR &= ~(1 << LED_GREEN_PIN);
  }
    // middle: JS1 X < 200 && >= 180  => blue
  if (analog19 < 200 && analog19>=180) {
      LED_BLUE_DOR &= ~(1 << LED_BLUE_PIN);
  }
    // right: JS1 X <180 && >= 0      => red
 if (analog19 < 180 && analog19>=0) {
      LED_RED_DOR &= ~(1 << LED_RED_PIN);
  }
 
 
 

    // Joystick left
    // green on
    // blue off
    // red off

    // Joystick middle
    // green off
    // blue on
    // red off

    // Joystick right
    // green off
    // blue off
    // red on

    // delay 0,01s
}

void printValues() {
    // Get analog values
  uint8_t analog11;
  uint8_t analog12;
  uint8_t analog13;
  uint8_t analog16;
  uint8_t analog19;
  uint8_t analog23;
  uint8_t analog17;
  cppp_getAnalogValues(&analog11, &analog12, &analog13, &analog16, &analog17, &analog19, &analog23);
  
    // Read and print all analog values of the system
  

    // Set cursor of the display to (480,320)
    setCursor(0, 260);

    // Write one linebreak then the headline, afterwards 4 linebreaks

    // Write analog values of joystick 2
   writeNumberOnDisplayRight(&analog13);
   writeText(" ");
   writeNumberOnDisplayRight(&analog23);
   
   writeText("\n");
    // Write analog values of joystick 1
   writeNumberOnDisplayRight(&analog16);
   writeText(" ");
   writeNumberOnDisplayRight(&analog19);

  writeText("\n");
    writeTextln("Joystick values");
}
