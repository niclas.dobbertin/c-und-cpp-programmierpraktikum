#include "init.h"
#include "blinkrainbow.h"
#include "acceleration_app.h"
#include "button.h"
#include "display.h"
#include "gfx.h"
#include "joystick.h"
#include "delay.h"
#include "touch.h"

int main() {
    initBoard();

    // BlinkRainbowMain();
    // ButtonToggleBlueLED();
    // ButtonHoldBlueLEDOn();
    
    // uint16_t result = color565(0x00, 0xEA,0xFF);
    // cppp_fillScreen(MAGENTA);
    printPattern(MAGENTA,YELLOW);
    initCursor();
    controlLedsInit();
    paintTouch();
    while (1) {
      printValues();
      controlLeds();
      debugTouch();
      cppp_microDelay(1000);
    }
    return 0;
}
