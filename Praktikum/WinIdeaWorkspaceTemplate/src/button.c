#include "button.h"
#include "pins.h"
#include "delay.h"

static uint8_t ledStatus;

static int isButtonPressed();

static void initLED() {
    // set LED status
    ledStatus = 0;
    // initialize PF5 as input and activate pullup resistor
    BUTTON_LEFT_DDR &= ~(1 << BUTTON_LEFT_PIN);
    BUTTON_LEFT_PCR |= (1 << BUTTON_LEFT_PIN);
    
    // initialize blue Led
    LED_BLUE_DDR |= (1 << LED_BLUE_PIN); // Configure blue LED pin as output.
    LED_BLUE_DOR |= (1 << LED_BLUE_PIN); // Turn LED off.
}

void ButtonToggleBlueLED() {
    // Initialization
    initLED();
    int pressed = 0;
    while (1) {
        // if button pressed toggle ledStatus and wait unitl button is released
        // set LED
       if (isButtonPressed()) {
          pressed = 1;
      }else if (pressed){
          LED_BLUE_DOR ^= (1 << LED_BLUE_PIN);
          pressed = 0;
      }
    }
}

void ButtonHoldBlueLEDOn() {
    // initialization
    initLED();
    uint32_t sleepTime = 1000;
    while (1) {
        // while button pressed turn LED on
        // delay of 0,01s
      if (isButtonPressed()) {
        LED_BLUE_DOR &= ~(1 << LED_BLUE_PIN);
        cppp_microDelay(sleepTime);
      }  else {
        LED_BLUE_DOR |= (1 << LED_BLUE_PIN);
        cppp_microDelay(sleepTime);
      }

        // normal case LED is off
        // delay of 0,01s
    }
}

/**
 * Returns true (1) if the button is pressed
 */
static int isButtonPressed() {
    // Return 1 if button is pressed, 0 otherwise.
    return (!(BUTTON_LEFT_DIR & (1 << BUTTON_LEFT_PIN)));
}

/**
 *  Changes the status of the blue LED.
 */
static void toggleBlueLED() {
    // invert value of LEDStatus
     
}

/**
 *  Sets the blue LED to its status.
 */
static void setBlueLED(uint8_t status) {
    // Set value register of blue LED to ledStatus
}
