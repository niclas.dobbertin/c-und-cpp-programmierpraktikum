#include "touch.h"

#include <stdint.h>
#include "gfx.h"
#include "lcd.h"
#include "display.h"
#include "analog.h"

#define BOXSIZE 40
#define PENRADIUS 50

static uint16_t oldcolor, currentcolor;

void debugTouch() {
    // Get analog values
    uint16_t x = cppp_readTouchX();
    uint16_t y = cppp_readTouchY();
    uint16_t z = cppp_readTouchZ();
    // Set cursor to (480,320)
    setCursor(0, 140);
    // Write headline on the display with 1 linebreak before and 4 afterwards

    // Get analog values of the touchscreen by using the readTouch methods

    // Write x,y, and z-values on the screen
    writeText("Z-Value: ");
    write16BitNumberOnDisplay(&z, 0);
    writeText("\n");
    writeText("Y-Value: ");
    write16BitNumberOnDisplay(&y, 0);
    writeText("\n");
    writeText("X-Value: ") ;
    write16BitNumberOnDisplay(&x, 0);
    writeText("\n");
    writeTextln("Touchscreen Values");
}
static void loopPaintTouch();
static void initPaintTouch();

void paintTouch() {
    initPaintTouch();

    while (1) {
        loopPaintTouch();
    }
}

/** 
 * Initializes the paint studio.
 */
static void initPaintTouch() {
    cppp_fillScreen(BLACK);
    cppp_fillRect(0, 0, BOXSIZE, BOXSIZE, RED);
    cppp_fillRect(BOXSIZE, 0, BOXSIZE, BOXSIZE, YELLOW);
    cppp_fillRect(BOXSIZE * 2, 0, BOXSIZE, BOXSIZE, GREEN);
    cppp_fillRect(BOXSIZE * 3, 0, BOXSIZE, BOXSIZE, CYAN);
    cppp_fillRect(BOXSIZE * 4, 0, BOXSIZE, BOXSIZE, BLUE);
    cppp_fillRect(BOXSIZE * 5, 0, BOXSIZE, BOXSIZE, MAGENTA);

    cppp_drawRect(0, 0, BOXSIZE, BOXSIZE, WHITE);
    currentcolor = RED;

    setCursor(260, 25);
    setTextSize(2);
    char text[] = "BILD ERNEUERN";
    writeText(text);
}

/**
 * Loop of paintTouch.
 */
static void loopPaintTouch() {
    // Save the actual touchpoint in the struct touchpoint
    struct touchPointStruct touchPoint;
    touchPoint.x = cppp_readTouchX();
    touchPoint.y = cppp_readTouchY();
    touchPoint.z = cppp_readTouchZ();
    // Correct touchpoint
    if (touchPoint.z > TOUCHZMIN && touchPoint.z < TOUCHZMAX) {
        // Erase
        if (touchPoint.x > 260 && touchPoint.y <= 31) {
           initPaintTouch();
        }
        // Change color
        if (touchPoint.y <= BOXSIZE) {
          if (touchPoint.x >= BOXSIZE*0 && touchPoint.x <= BOXSIZE * 1 ){
            currentcolor = RED;
          }
          if (touchPoint.x >= BOXSIZE*1 && touchPoint.x <= BOXSIZE * 2 ){
            currentcolor = YELLOW;
          }
          if (touchPoint.x >= BOXSIZE*2 && touchPoint.x <= BOXSIZE * 3 ){
            currentcolor = GREEN;
          }
          if (touchPoint.x >= BOXSIZE*3 && touchPoint.x <= BOXSIZE * 4 ){
            currentcolor = CYAN;
          }
          if (touchPoint.x >= BOXSIZE*4 && touchPoint.x <= BOXSIZE * 5 ){
            currentcolor = BLUE;
          }
          if (touchPoint.x >= BOXSIZE*5 && touchPoint.x <= BOXSIZE * 6 ){
            currentcolor = MAGENTA;
          }
              
         }else {
        // Draw a point on the screen
        cppp_fillCircle(touchPoint.x, touchPoint.y, (touchPoint.z-100)/10, currentcolor);
        }
    }
}
