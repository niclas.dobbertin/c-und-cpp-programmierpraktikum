#ifndef TOUCH_H
#define TOUCH_H
#include <stdint.h>
/**
 * Prints the x,y and z-value of the touchscreen on the display.
 */
void debugTouch();
void debugTouch_s();


/**
 * Painting application
 */
void paintTouch();
void paintTouch_s();

struct touchPointStruct {
  uint16_t x;
  uint16_t y;
  uint16_t z;
} ;
#endif
