#include <stdio.h>

#include "registers.h"

Register_t ADCCON3 = 0xa14c99f7;

void printRegister(Register_t reg) {
    printf("0b");

    for (int i = 31; i >= 0; --i) {
        if (reg & (1 << i)) {
            printf("1");
        }
        else {
            printf("0");
        }
    }

    printf("\n");
}

#define DIGEN7 (1 << 23)
#define DIGEN6 (1 << 22)
#define DIGEN0TO5 (0b111111 << 16)
#define CONCLKDIV (0b111111 << 24)
#define SIXTY_TIMES_TCLK (0b011110 << 24)
#define VREFSEL (0b111 << 13)

void setRegisters(Register_t* reg) {
    // ADC 6 & 7
    *reg = (*reg & ((~DIGEN0TO5)));
    *reg = (*reg | DIGEN6);
    *reg = (*reg | DIGEN7);

    // Tq divider
    *reg = (*reg & ((~CONCLKDIV)));
    *reg = (*reg | SIXTY_TIMES_TCLK);

    // AD_ref
    *reg = (*reg & ((~VREFSEL)));
    *reg = (*reg | (0b110 << 13));

}
struct ADCCON3Reg {
    uint32_t ignore1: 13;
    uint32_t vrefsel: 3;
    uint32_t digen: 8;
    uint32_t conclkdiv: 6;
    uint32_t ignore2: 2;
};

void setRegistersStruct(Register_t* _reg) {
    struct ADCCON3Reg* reg = _reg;
    reg->vrefsel = 0b110;
    reg->digen = 0b11000000;
    reg->conclkdiv = 0b011110;
}
