#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "strings.h"

static int is_prefix(const char *src, const char *pattern)
{
	if (strlen(pattern) > strlen(src))
		return 0;

	/* Check if any char doesn't match */
	for (unsigned int j = 0; j < strlen(pattern); j++) {
		if (pattern[j] != src[j]) {
			return 0;
		}
	}
	return 1;
}

int split(char **dest, const char *src, const char *splitStr)
{
	int dest_index = 0;
	char *substring = malloc(strlen(src) * sizeof(char));
	substring[0] = '\0';

	for (unsigned int i = 0; i < strlen(src); i++) {
		int match = is_prefix(&src[i], splitStr);
		if (match) {
			// write to dest
			char *dest_element =
				malloc(strlen(substring) * sizeof(char));
			strcpy(dest_element, substring);
			dest[dest_index] = dest_element;

			// reset substring
			substring[0] = '\0';
			dest_index++;
			i = i + strlen(splitStr) - 1;
		} else {
			// append current char to substring
			size_t sub_len = strlen(substring);
			substring[sub_len] = src[i];
			substring[sub_len + 1] = '\0';
		}
	}
	// write last substring after loop to dest
	dest[dest_index] = malloc(strlen(substring) * sizeof(char));
	sprintf(dest[dest_index], "%s", substring);
	dest_index++;

	free(substring);
	return dest_index;
}

char *concatenate(const char *a, const char *b)
{
	int len_sum = strlen(a) + strlen(b);
	char *concat = malloc(len_sum * sizeof(char));
	strcpy(concat, a);
	strcpy(&concat[strlen(a)], b);

	return concat;
}
