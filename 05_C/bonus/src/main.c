#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "registers.h"
#include "strings.h"

int main(int argc, char **argv)
{
	char *concat = concatenate("Hello", "World\n");
	printf("String: %s", concat);
	free(concat);

	char **buffer = (char **)malloc(32);

	int split_len = split(buffer, "hello;;world;;!", ";;");
	for (int i = 0; i < split_len; i++) {
		printf("Element %d: %s\n", i, buffer[i]);
        free(buffer[i]);
	}
    free(buffer);

	printRegister(ADCCON3);

#ifdef USE_STRUCTS
	setRegistersStruct(&ADCCON3);
	printf("used structs\n");
#else
	setRegisters(&ADCCON3);
#endif
	printRegister(ADCCON3);
}
