#include "Vector3.hpp"

int main() {
    Vector3 p = Vector3();
    Vector3 q(p);
    p = Vector3(1, 2, 3);
    std::cout << p << q << std::endl;
    Vector3 v = Vector3(1, 2, 3) + Vector3(4, 5, 6) - Vector3(7, 8, 9);
    std::cout << v << std::endl;
}
