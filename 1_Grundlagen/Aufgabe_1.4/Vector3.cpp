#include "Vector3.hpp"

using namespace std;

Vector3::Vector3() : a(0), b(0), c(0) {
    cout << "Default Constructor" << endl;
}

Vector3::Vector3(double a, double b, double c) : a(a), b(b), c(c) {
    cout << "Parameter Constructor" << endl;
}

Vector3::Vector3(const Vector3 &other) : a(other.a), b(other.b), c(other.c) {
    cout << "Copy Constructor" << endl;
}

Vector3::~Vector3() {
    cout << "Destructor" << endl;
}

double Vector3::getA() {return a;}

double Vector3::getB() {return b;}

double Vector3::getC() {return c;}

Vector3 Vector3::operator+(Vector3 rhs) {return {a + rhs.a, b + rhs.b, c + rhs.c};}

Vector3 Vector3::operator-(Vector3 rhs) {return {a - rhs.a, b - rhs.b, c - rhs.c};}

double Vector3::operator*(Vector3 rhs) {return a * rhs.a + b * rhs.b + c * rhs.c;}

std::ostream& operator<<(std::ostream& out, Vector3& rhs) {
    out << "(" << rhs.getA() << ", " << rhs.getB() << ", " << rhs.getC() << ")";
    return out;
}
