#include <iostream>
#include "functions.hpp"

int main() {
    printStars(50);
    printDataTypes();
    printStars(50);

    printFigure();
    printStars(50);
    printCharFigure();
    printStars(50);

    MarketItem banana {200, 1.5};
    MarketItem apple {150, 0.75};
    MarketItem watermelon {1000, 4.0};
    std::cout << "Gewicht einer Banane: " << banana.weight << "g" << std::endl;
    std::cout << "Preis einer Banane: " << banana.price << "E" << std::endl;
}
