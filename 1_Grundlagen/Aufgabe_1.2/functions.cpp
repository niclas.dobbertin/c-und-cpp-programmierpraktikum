#include <iostream>
#include <limits>
#include "functions.hpp"

using namespace std;

static char c = 'a';

int printDataTypes() {
    cout << "DATA TYPES" << endl;

    cout << "- int: " << sizeof(int) * 8 << " Bit";
    cout << ", min=" << numeric_limits<int>::min();
    cout << ", max=" << numeric_limits<int>::max() << endl;

    cout << "- unsigned int: " << sizeof(unsigned int) * 8 << " Bit";
    cout << ", min=" << numeric_limits<unsigned int>::min();
    cout << ", max=" << numeric_limits<unsigned int>::max() << endl;

    cout << "- double: " << sizeof(double) * 8 << " Bit";
    cout << ", min=" << numeric_limits<double>::min();
    cout << ", max=" << numeric_limits<double>::max() << endl;

    cout << "- unsigned short: " << sizeof(unsigned short) * 8 << " Bit";
    cout << ", min=" << numeric_limits<unsigned short>::min();
    cout << ", max=" << numeric_limits<unsigned short>::max() << endl;

    cout << "- bool: " << sizeof(bool) * 8 << " Bit";
    cout << ", min=" << numeric_limits<bool>::min();
    cout << ", max=" << numeric_limits<bool>::max() << endl;

    return 0;
}

int printStars(int n) {
    for (int i = n; i > 0; i--)
        cout << "*";
    cout << endl;

    return 0;
}

int printSpaces(int n) {
    for (int i = n; i > 0; i--)
        cout << " ";

    return 0;
}

int inputFigureWidth() {
    int width;

    do {
        cout << "Width of figure (max 80): ";
        cin >> width;
        if (width > 80) cout << "Maximum width is 80! Please provide another width." << endl;
    } while (width > 80);

    return width;
}

Direction inputFigureDirection() {
    int input;

    do {
        cout << "Direction of figure (Left = 0, Right = 1): ";
        cin >> input;
    } while (input > 1);

    return input == 0 ? Left : Right;
}

int printFigure() {
    int n = inputFigureWidth();
    Direction d = inputFigureDirection();

    for (int i = n; i > 0; i--) {
        if (d == Right) printSpaces(n - i);
        printStars(i);
    }
    for (int i = 2; i <= n; i++) {
        if (d == Right) printSpaces(n - i);
        printStars(i);
    }

    return 0;
}

char nextChar() {
    char current = c++;
    if (c > 'z') c = 'a';
    return current;
}

void printChars(int n) {
    for (int i = n; i > 0; i--)
        cout << nextChar();
    cout << endl;
}

void printCharFigure() {
    int n = inputFigureWidth();
    Direction d = inputFigureDirection();

    for (int i = n; i > 0; i--) {
        if (d == Right) printSpaces(n - i);
        printChars(i);
    }
    for (int i = 2; i <= n; i++) {
        if (d == Right) printSpaces(n - i);
        printChars(i);
    }
}
