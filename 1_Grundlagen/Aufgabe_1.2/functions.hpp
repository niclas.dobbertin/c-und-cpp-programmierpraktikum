#ifndef FUNCTIONS_HPP_
#define FUNCTIONS_HPP_

/**
 * @file functions.hpp
 * @author Arne Turuc
 * @brief A collection of functions to print data types and stars to the console.
 */

/**
 * @author Arne Turuc
 * @brief Two possible directions indicating the alignment of the printed stars.
 */
enum Direction {Left, Right};

/**
 * @author Arne Turuc
 * @brief A MarketItem struct with corresponding weight and price tags.
 */
struct MarketItem {
    int weight;
    float price;
};

/**
 * @author Arne Turuc
 * @brief Prints common data types along with their bit sizes and min and max values.
 * @return 0 if print was successful
 */
int printDataTypes();

/**
 * @author Arne Turuc
 * @brief Prints a line of stars.
 * @param n Number of stars to be printed
 * @return 0 if print was successful
 */
int printStars(int n);

/**
 * @author Arne Turuc
 * @brief Prints multiple space characters without a line break.
 * @param n Number of spaces to be printed
 * @return 0 if print was successful
 */
int printSpaces(int n);

/**
 * @author Arne Turuc
 * @brief Gets figure width from input, max value 80.
 * @return The width as provided by the input
 */
int inputFigureWidth();

/**
 * @author Arne Turuc
 * @brief Gets figure direction from input, between Left or Right.
 * @return The direction as provided by the input
 */
Direction inputFigureDirection();

/**
 * @author Arne Turuc
 * @brief Prints a figure consisting of stars, aligned to a direction.
 * @return 0 if print was successful
 */
int printFigure();

/**
 * @author Arne Turuc
 * @brief Iterates over the english alphabet, providing a char at each call.
 * @return The next char to be printed
 */
char nextChar();

/**
 * @author Arne Turuc
 * @brief Prints the next n chars specified by nextChar() as a line.
 * @param n Number of chars to be printed
 */
void printChars(int n);

/**
 * @author Arne Turuc
 * @brief Prints a figure consisting of the english alphabet, aligned to a direction.
 */
void printCharFigure();

#endif //FUNCTIONS_HPP_
