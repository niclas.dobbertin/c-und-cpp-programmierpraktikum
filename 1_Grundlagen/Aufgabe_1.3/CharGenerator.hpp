#ifndef CHARGENERATOR_HPP_
#define CHARGENERATOR_HPP_

/**
 * @class CharGenerator
 * @author Arne Turuc
 * @brief A class generating a char sequence.
 */

class CharGenerator {
public:
    /**
     * @author Arne Turuc
     * @brief Constructor of the class, initialize with 'a'.
     */
    explicit CharGenerator(char initialChar = 'a');

    /**
     * @author Arne Turuc
     * @brief Generates the next char in the sequence.
     * @return The next char
     */
    char generateNextChar();
private:
    char nextChar;
};

#endif //CHARGENERATOR_HPP_
