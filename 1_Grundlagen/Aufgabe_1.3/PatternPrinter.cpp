#include <iostream>
#include "PatternPrinter.hpp"

using namespace std;

PatternPrinter::PatternPrinter() = default;

void PatternPrinter::printPattern() {
    int n = readWidth();

    for (int i = n; i > 0; i--) printNChars(i);
    for (int i = 2; i <= n; i++) printNChars(i);
}

void PatternPrinter::printNChars(int n) {
    for (int i = 0; i < n; i++) cout << charGen.generateNextChar();
    cout << endl;
}

int PatternPrinter::readWidth() {
    int width;

    do {
        cout << "Width of the Pattern: ";
        cin >> width;
    } while (width < 0);

    return width;
}
