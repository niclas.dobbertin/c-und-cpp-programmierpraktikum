cmake_minimum_required(VERSION 3.21)
project(c_und_cpp_programmierpraktikum)

set(CMAKE_CXX_STANDARD 14)

add_executable(Aufgabe_1.3 main.cpp CharGenerator.hpp CharGenerator.cpp PatternPrinter.hpp PatternPrinter.cpp)