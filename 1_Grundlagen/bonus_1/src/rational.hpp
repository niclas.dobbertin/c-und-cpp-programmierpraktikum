#pragma once

#include <iostream>

class Rational {
public:
    // constructors and destructor
    Rational();
    Rational(int counter, int denominator);
    Rational(const Rational& other);
    ~Rational();

    // operator overloading
    Rational operator+(Rational rhs);
    Rational operator-(Rational rhs);
    Rational operator*(Rational rhs);
    Rational operator/(Rational rhs);
    bool operator<(Rational rhs);

    // attributes
    int counter, denominator;

std::ostream& operator<<(std::ostream& out, Rational rhs);

// simplify fraction
Rational simplify(Rational rhs);

// calculate the greatest common divisor (gcd) using recursion
int getGCD(int a, int b);
