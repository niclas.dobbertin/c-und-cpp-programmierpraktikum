#include "rational.hpp"
#include <iostream>
#include <cmath>

using namespace std;

// constructor without parameters
Rational::Rational() : counter(1), denominator(2) {
    cout << "(Rational 1/2 created)" << endl;
}

// constructor with parameters
Rational::Rational(int counter, int denominator) : counter(counter), denominator(denominator) {
    if (denominator == 0) throw invalid_argument("denominator must not be 0");
    cout << "(Rational " << counter << "/" << denominator << " created)" << endl;
}

// copy constructor
Rational::Rational(const Rational &other) : counter(other.counter), denominator(other.denominator) {
    cout << "(Rational " << counter << "/" << denominator << " copied)" << endl;
}

// destructor
Rational::~Rational() {
    cout << "(Rational " << counter << "/" << denominator << " destroyed)" << endl;
}

// "+" operator overloading
Rational Rational::operator+(Rational rhs) {
    int c = counter * rhs.denominator + rhs.counter * denominator;
    int d = denominator * rhs.denominator;
    return {c, d};
}

// "-" operator overloading
Rational Rational::operator-(Rational rhs) {
    int c = counter * rhs.denominator - rhs.counter * denominator;
    int d = denominator * rhs.denominator;
    return {c, d};
}

// "*" operator overloading
Rational Rational::operator*(Rational rhs) {
    int c = counter * rhs.counter;
    int d = denominator * rhs.denominator;
    return {c, d};
}

// "/" operator overloading
Rational Rational::operator/(Rational rhs) {
    int c = counter * rhs.denominator;
    int d = denominator * rhs.counter;
    return {c, d};
}

// "<" operator overloading
bool Rational::operator<(Rational rhs) {
    double r1 = (double) counter / (double) denominator;
    double r2 = (double) rhs.counter / (double) rhs.denominator;
    return r1 < r2;
}

// "<<" operator overloading
std::ostream& operator<<(std::ostream& out, Rational rhs) {
    return out << rhs.counter << "/" << rhs.denominator;
}

// recursive euclidean algorithm https://en.wikipedia.org/wiki/Euclidean_algorithm
int getGCD(int a, int b) {
    return b ? getGCD(b, a % b) : a;
}

Rational simplify(Rational rhs) {
    int c = rhs.counter;
    int d = rhs.denominator;

    // Eliminate negative denominator by multiplying by -1
    if (d < 0) {c = -c; d = -d;}

    // Find the greatest common divisor using euclid
    int GCD = getGCD(abs(c), abs(d));

    // Return by gcd reduced rational
    return {c / GCD, d / GCD};
}
