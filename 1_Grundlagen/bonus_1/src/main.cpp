#include <iostream>
#include "rational.hpp"

using namespace std;

int main(int argc, char **argv) {
    // create fractions a = 1/4 and b = 1/2
    cout << "---- Create a = 1/4 and b = 1/2 ----" << endl;
    Rational a(1, 4);
    Rational b;  // calls default constructor

    // calculate fractions c, d, e, f
    cout << "---- Calculate c, d, e, f ----" << endl;
	Rational c = b + a;
	cout << c << endl;
	Rational d = b - a;
	cout << d << endl;
	Rational e = b * a;
	cout << e << endl;
	Rational f = b / a;
	cout << f << endl;

    // calculate smallest fraction
    cout << "---- Determine smallest among c, d, e, f ----" << endl;
    Rational smallest = c;
    if (d < smallest) smallest = d;
    if (e < smallest) smallest = e;
    if (f < smallest) smallest = f;
    cout << "The smallest fraction is " << smallest << endl;

    // simplify all previously computed fractions
    cout << "---- Simplify c, d, e, f ----" << endl;
	c = simplify(c);
	cout << c << endl;
	d = simplify(d);
	cout << d << endl;
	e = simplify(e);
	cout << e << endl;
	f = simplify(f);
	cout << f << endl;

    cout << "---- Destroy all objects still left ----" << endl;
}
